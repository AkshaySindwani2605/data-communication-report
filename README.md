Please check the Downloads section of the repository to download the report. 
This report was made aa a part of a research assesment conducted in Monash University. The intent of the report is to build a floor plan and do a
comparative analysis of **three SSIDs (access points) from ten different locations**. The findings of the report discuss the 
**Channel Occupancy, Interference, Attentuation and Coverage** of the readings taken from ten different locations of the three access points.